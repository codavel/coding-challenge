# Cache

### Problem Description

After each Bolina connection, both client and server store some relevant information to improve the performance and efficiency of future connections. That information is stored in a object that we called Cache, which is unique for each pair Client/Server.

Since each server can have several hundreds of clients, we had to limit the number of Cache objects in memory, while ensuring that the time it takes to access an available Cache would not had any impact on the connection time.

How would you implement the Class that stores the cache objects?

### Requirements

* The number of Caches available in memory must be limited. When full, adding a new one will delete the oldest Cache, based on the creation timestamp;
* Instant access to every available Cache.
* Each Cache can store any type of information (String, Long, ...);
* Allow the update of the Cache's data. The oldest Cache in memory will be defined based on the update timestamp.


### Input/Output Example

    size = 3
    AvailableCaches caches(size)
    Cache cache_1("one")
    Cache cache_2("two")
    Cache cache_3("three")
    Cache cache_4("four")

    caches.add(cache_1)
        > ok
    caches.print()
        > "one"
    
    caches.add(cache_2)
        > ok
    caches.print()
        > "one", "two"

    caches.add(cache_3)
        > ok
    caches.print()
        > "one", "two", "three"

    caches.add(cache_4)
        > ok
    caches.print()
        > "two", "three", "four"

    caches.get(cache_2).update("two_updated")
            > ok
    caches.print()
            > "three", "four", "two_updated"

    


